﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Von
{
    public class Produkt
    {
        private int id;
        private string type;

        public Produkt(int id, string type)
        {
            this.id = id;
            this.type = type;
        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Typ
        {
            get { return type; }
            set { type = value; }

        }
    }
}
