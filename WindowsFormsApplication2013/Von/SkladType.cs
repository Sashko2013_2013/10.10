﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Von
{
    public class SkladType
    {
        private int id;
        private int count;
        public List<Produkt> arrProd;
        public SkladType(int id, int count)
        {
            this.id = id;
            this.count = count;
            arrProd = new List<Produkt>();
        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public int Count
        {
            get { return count; }
            set { count = value; }
        }
       
    }
}
