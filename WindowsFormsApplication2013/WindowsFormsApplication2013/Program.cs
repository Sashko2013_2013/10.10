﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Von;
public delegate void MyDelegate(string data);

namespace WindowsFormsApplication2013
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
