﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Von;

namespace WindowsFormsApplication2013
{
    public partial class Form1 : Form
    {
        public List<Produkt> Produkt;
        SkladType skl;
        public Form1()
        {
            InitializeComponent();
            Produkt = new List<Produkt>();
            richTextBox1.Enabled = false;
            Sklad();
        }

        private void Sklad()
        {
            Produkt.Add(new Produkt(1, "Доска 2Х7"));
            Produkt.Add(new Produkt(2, "Брус"));
            Produkt.Add(new Produkt(3, "Доска"));
            Produkt.Add(new Produkt(4, "Доска 3Х5"));
            Produkt.Add(new Produkt(5, "Бревнo 4x6"));
            Produkt.Add(new Produkt(6, "ДВП 4x4"));
            Produkt.Add(new Produkt(7, "ДСП 3x4"));
            Produkt.Add(new Produkt(8, "ДВП 3Х4"));
            Produkt.Add(new Produkt(9, "ДВП 3Х9"));
            Produkt.Add(new Produkt(10, "ДВП 3Х45"));
            Produkt.Add(new Produkt(11, "Доска 30Х50"));
            Produkt.Add(new Produkt(12, "Бревнo 40x60"));
            Produkt.Add(new Produkt(13, "ДВП"));
            Produkt.Add(new Produkt(14, "Метал"));
            Produkt.Add(new Produkt(15, "Дерево"));
            Produkt.Add(new Produkt(16, "Пластмас"));
            Produkt.Add(new Produkt(17, "Резина"));
            Produkt.Add(new Produkt(18, "Скло"));
            Produkt.Add(new Produkt(19, "Скло-вата"));
        }
        private void Table_table()
        {
            DataTable Table = new DataTable("Table");
            DataColumn Id_table = new DataColumn("Id");
            DataColumn Type_table = new DataColumn("Type");

            Table.Columns.Add(Id_table);
            Table.Columns.Add(Type_table);

            foreach (Produkt resprod in Produkt)
            {
                DataRow _newRowForProdukt;
                _newRowForProdukt = Table.NewRow();
                _newRowForProdukt["Id"] = resprod.Id;
                _newRowForProdukt["Type"] = resprod.Typ;
                Table.Rows.Add(_newRowForProdukt);
            }
            dataGridView1.DataSource = Table;
        }
        public void Find()
        {
            try
            {
                int tem_id;

                tem_id = int.Parse(textBox1.Text);
                var Query = from c in Produkt
                            where c.Id == tem_id
                            select new
                            {
                                ID = c.Id,
                                Type = c.Typ
                            };
                foreach (var num in Query)
                    richTextBox1.Text += "ID продукта: " + num.ID + "\n" + "Тип продукта: " + num.Type + "\n";
            }
            catch
            {
                richTextBox1.Text = "ОШИБКА! ";
            }
        }
        public void Find_type()
        {
            try
            {
                string qw_type;
                qw_type = textBox2.Text;
                var Query = from a in Produkt
                            where a.Typ == qw_type
                            select new
                            {
                                ID = a.Id,
                                Type = a.Typ
                            };
                foreach (var num in Query)
                    
                    richTextBox1.Text += "Поиск по Типу:" + "\n" + "ID: " + num.ID + "\n" + "Тип: " + num.Type + "\n" + "\n";
            }

            catch
            {
                richTextBox1.Text = "Ошибка!";
            }
        }

       private void Form1_Load(object sender, EventArgs e)
        {
            //MessageBox.MessageBox.Show("");
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Find();
            Find_type();


        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //vivod_dan();
            Table_table();
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "Количеcво-товаров:" + "  " + Produkt.Count.ToString();
        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Poisk_Click(object sender, EventArgs e)
        {
            Form frm = new Poisk_Form();
            frm.Show();
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
       
    

