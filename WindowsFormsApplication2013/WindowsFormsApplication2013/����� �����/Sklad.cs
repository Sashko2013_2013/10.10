﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication2013
{
   public class Sklad
    {
        private int id;
        private int count;
        public List<Produkt> arrProd;
        public int Id 
        {
            get
            {
                return id; 
            }
            set
            {
                id = value; 
            }
        }
        public int Count
        {
            get
            {
                return count;
            }
            set
            {
                count = value;
            }
        }
       public Sklad(int id,int count)
      {
         this.id = id;
         this.count = count;
         arrProd = new List<Produkt>();   
      }

        }
}